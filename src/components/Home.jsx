import React from 'react';
import { BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import App from "../App";
import Dashboard from "./Dashboard"
import About from "./About"
import SignIn from './SignIn';

export default () => (
    <Router >
        <Switch>
            <Route exact path="/" >
                <App />
            </Route>
            <Route path="/about">
                <About />
            </Route>
            <Route path="/dashboard">
                <Dashboard />
            </Route>
            <Route path="/signin">
                <SignIn />
            </Route>
        </Switch>
    </Router>
)