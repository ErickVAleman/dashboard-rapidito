const data1 = {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
        datasets: [{
            label: 'My First dataset',
            borderColor: "#590202",
            backgroundColor: ["#590202","#3cb371",  
            "#0000FF",  
            "#9966FF",  
            "#4C4CFF",  
            "#00FFFF",  
            "#f990a7",  
            "#aad2ed",  
            "#FF00FF",],
            data: [10, 30, 46, 2, 8, 50, 0],
            fill: false,
        }, {
            label: 'My Second dataset',
            borderColor: "rgba(255, 237, 75, 1)",
            backgroundColor: "rgba(255, 237, 75, 1)",
            data: [7, 49, 46, 13, 25, 30, 22],
            fill: false,
        }]
};

const data2 = {
    labels: [ 'uno', 'dos', 'tres', 'cuatro', 'cinco', 'seis', 'siete' ],
    datasets: [
        {
            label: 'Label del grafico de prueba',
            data: ['10', '20', '30', '12', '15', '25', '30'],
            backgroundColor: [  
                'rgba(255, 237, 75, 0.2)',
                ],
                borderColor: [
                    'rgba(255, 237, 75, 1)',
                    
                ],
                borderWidth: 1
        }
    ]
  };

  const dataPie = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    datasets: [{
        label: 'My First dataset',
        borderColor: "#590202",
        backgroundColor: ["#590202","#3cb371",  
        "#0000FF",  
        "#9966FF",  
        "#4C4CFF",  
        "#00FFFF",  
        "#f990a7",  
        "#aad2ed",  
        "#FF00FF",],
        data: [10, 30, 46, 2, 8, 50, 0],
        fill: false,
    }]
};

const dataPolar = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    datasets: [{
        label: 'My First dataset',
        borderColor: "#590202",
        backgroundColor: ["#590202","#3cb371",  
        "#0000FF",  
        "#9966FF",  
        "#4C4CFF",  
        "#00FFFF",  
        "#f990a7",  
        "#aad2ed",  
        "#FF00FF",],
        data: [10, 30, 46, 2, 8, 50, 0],
        fill: false,
    }]
};

  export {
      data1,
      data2,
      dataPie,
      dataPolar
  }